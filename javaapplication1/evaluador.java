package javaapplication1;
//JOSE ALEJANDRO COLQUE CALLAHUARA
//CI:859802
//R.U:76542
public class evaluador {
    public static double evaluar(String infija){
        String posfija = convertir(infija);
        System.out.println("expresion posfija es:"+posfija);
        return evaluarPosfija(posfija);
    }

    private static String convertir(String infija) {
       String posfija = "" ;
       pila Pila =new pila(100);
       for(int i=0;i< infija.length();i++){
           char letra =infija.charAt(i);
           if(esoperador(letra)) {
             if(Pila.estaVacia()){
                Pila.apilar(letra);
             }else{
                 int pe = prioridadEnExpresion(letra);
                 int pp = prioridadEnPila((char)Pila.elementoTope());
                 if (pe>pp){
                   Pila.apilar(letra);  
                 }else{
                 posfija += Pila.desapilar();
                 Pila.apilar(letra);
             }
                 
             }
             }else {
               posfija += letra;
           }
       }
       while(!Pila.estaVacia()){
           posfija += Pila.desapilar();
       }
       return posfija;
    }
    private static int prioridadEnExpresion(char operador)
    {
        if(operador == '*') return 4;
        if(operador == '*' || operador == '/' ) return 2;
        if(operador == '+' || operador == '-' ) return 1;
         if(operador == '(' ) return 5;
        return 0;
        
    } private static int prioridadEnPila(char operador)
    {
        if(operador == '*') return 3;
        if(operador == '*' || operador == '/' ) return 2;
        if(operador == '+' || operador == '-' ) return 1;
        if(operador == '(' ) return 0;
        return 0;
        
    }
    private static double evaluarPosfija(String posfija) {
      pila Pila = new pila(100);
      for(int i = 0;i < posfija.length();i++){
         char letra =posfija.charAt(i);
         if(!esoperador(letra)){
            double num = new Double(letra + " ");
            Pila.apilar(num);
         }else{
             double num2 =(double)Pila.desapilar();
             double num1 =(double)Pila.desapilar();
             double num3 = operacion(letra,num1,num2);
             Pila.apilar(num3);
         }
      }
      return(double)Pila.elementoTope();
    }

    private static boolean esoperador(char letra) {
        if (letra == '*' || letra == '/' || letra == '+' || letra == '-' || letra == '('|| letra == ')'|| letra == '^'   ){
            return true;       
        }   
        return false;
    }

    private static double operacion(char letra, double num1, double num2) {
        if(letra =='*')return num1*num2;
         if(letra =='/')return num1/num2;
          if(letra =='+')return num1+num2;
           if(letra =='-')return num1-num2;
            if(letra =='*')return Math.pow(num1,num2);
            return 0;
    }
}
